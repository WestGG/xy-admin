import Enum from 'xy-enum'
import {
    columns_0_0,
    columns_0_1,
    columns_1_0,
    columns_1_1,
    columns_1_2,
    columns_2_0,
    columns_3_0,
    columns_3_1,
} from '@/enums/columns'
// 菜单类型
export const menuTypeEnum = new Enum([
    { key: 'menu', value: 1, desc: '菜单' },
    { key: 'button', value: 2, desc: '按钮' },
])
export const dataType = {
    YEAR: '年份',
    QUARTER: '季度',
    MONTH: '月份',
}
export const industryEnum = {
    table_0: {
        title: '规模以上工业',
        children: {
            list_0: { type: 'YEAR', title: '年报', detailColumn: [columns_0_0], reportColumn: [] },
            list_1: { type: 'MONTH', title: '月报', detailColumn: [columns_0_1] },
        },
    },
    table_1: {
        title: '限额以上批发零售业',
        children: {
            list_0: { type: 'QUARTER', title: '季报', detailColumn: [columns_1_0] },
            list_1: { type: 'MONTH', title: '月报', detailColumn: [columns_1_1, columns_1_2] },
        },
    },
    table_2: {
        title: '限额以上住宿和餐饮业',
        children: {
            list_0: { type: 'QUARTER', title: '季报', detailColumn: [columns_3_0] },
            list_1: { type: 'MONTH', title: '月报', detailColumn: [columns_3_1] },
        },
    },
    table_3: {
        title: '规模以上服务业',
        children: {
            list_1: { type: 'MONTH', title: '月报', detailColumn: [columns_2_0] },
        },
    },
    table_4: {
        title: '房地产开发经营',
        children: { list_0: { type: 'YEAR', title: '年报', detailColumn: [columns_0_0] } },
    },
    table_5: {
        title: '有资质建筑业',
        children: { list_0: { type: 'YEAR', title: '年报', detailColumn: [columns_0_0] } },
    },
}
