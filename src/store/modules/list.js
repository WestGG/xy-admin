import { message } from 'ant-design-vue'
import { cloneDeep, findIndex, isEmpty } from 'lodash-es'
import { config } from '@/config'
import { defineStore } from 'pinia'
import apis from '@/apis'
const form1 = ['inventory', 'assets', 'produceCost', 'income', 'laborCost', 'other']
const useListStore = defineStore('listData', {
    state: () => ({
        detail: {},
        reportInfo: {},
    }),
    getters: {
        getDetail: (state) => state.detail,
        getReportInfo: (state) => state.reportInfo,
        getItems: (state) => form1.map((i) => ({ ...state.detail[i] })),
    },
    actions: {
        /**
         * 登录
         * @param {object} detail
         * @returns {void}
         */
        setDetail(detail) {
            this.detail = { ...this.detail, ...detail }
        },
        setReportInfo(reportInfo) {
            this.reportInfo = { ...this.reportInfo, ...reportInfo }
        },
    },
})

export default useListStore
