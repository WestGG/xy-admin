import zhCN from 'ant-design-vue/es/locale/zh_CN'

const configProviderAttrs = {
    locale: zhCN,
    theme: {
        token: {
            borderRadius: 4,
            borderRadiusSM: 2,
            fontSize: 16,
            // size: 18,
            colorPrimary: '#5364bb',
            colorSuccess: '#19a589',
            colorInfo: '#5364bb',
        },
        components: {
            List: {
                paddingContentHorizontalLG: 0,
            },
            Table: {
                paddingContentVerticalLG: 12,
                padding: 12,
            },
            Card: {
                paddingLG: 16,
            },
        },
    },
}
const useConfigProvider = () => ({ configProviderAttrs })
export default useConfigProvider
