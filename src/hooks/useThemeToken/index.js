import { theme } from 'ant-design-vue'
const useThemeToken = () => {
    const { useToken } = theme
    const token = useToken()
    return token
}
export default useThemeToken
