const industryNames = [
    '规模以上工业',
    '限额以上批发零售业',
    '限额以上住宿和餐饮业',
    '规模以上服务业',
    '房地产开发经营',
    '有资质建筑业',
]
/** 规模以上工业 - 年报 */
const columns_0_0 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '本年数据', key: 'currentValue', dataIndex: 'currentValue', align: 'right', color: '#5364bb' },
    { title: '财务报表数据', key: 'reportValue', dataIndex: 'reportValue', align: 'right', color: '#000' },
    { title: '差异数', key: 'diffValue', dataIndex: 'diffValue', align: 'right', color: '#f50' },
    { title: '差异率', key: 'diffRate', dataIndex: 'diffRate', align: 'right', color: '#f50' },
    { title: '行业水平', key: 'industryValue', dataIndex: 'industryValue', align: 'right' },
    { title: '全区水平', key: 'areaValue', dataIndex: 'areaValue', align: 'right' },
    { title: '审核备注', width: 240, key: 'remark', dataIndex: 'remark', align: 'left' },
]
/** 规模以上工业 - 月报 */
const columns_0_1 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    {
        title: '本月数据',
        key: 'currentValue',
        dataIndex: 'currentValue',
        align: 'right',
        color: '#5364bb',
    },
    { title: '财务报表数据', key: 'reportValue', dataIndex: 'reportValue', align: 'right', color: '#000' },
    { title: '差错数', key: 'diffValue', dataIndex: 'diffValue', align: 'right', color: '#f50' },
    { title: '差错率', key: 'diffRate', dataIndex: 'diffRate', align: 'right', color: '#f50' },
    { title: '审核备注', width: 240, key: 'remark', dataIndex: 'remark', align: 'left', color: '#f50' },
]
/** 限额以上批发零售业 - 季报 */
const columns_1_0 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    { title: '本季', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
    { title: '上年同期', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
]
/** 限额以上批发零售业 - 月报 - 表1 */
const columns_1_1 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    {
        title: '本年',
        align: 'center',
        children: [
            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
        ],
    },
    {
        title: '上年同期',
        align: 'center',
        children: [
            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center' },
            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center' },
        ],
    },
]
/** 限额以上批发零售业 - 月报 - 表2 */
const columns_1_2 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    {
        title: '商品销售额',
        align: 'center',
        children: [
            {
                title: '本年',
                align: 'center',
                children: [
                    { title: '本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
                    { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
                ],
            },
            {
                title: '上年同期',
                align: 'center',
                children: [
                    { title: '本月', key: '', dataIndex: 'currentValue', align: 'center' },
                    { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center' },
                ],
            },
            {
                title: '零售额',
                align: 'center',
                children: [
                    {
                        title: '本年',
                        children: [
                            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center' },
                            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center' },
                        ],
                    },
                    {
                        title: '上年同期',
                        children: [
                            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center' },
                            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center' },
                        ],
                    },
                ],
            },
        ],
    },
]
/** 规模以上服务业 - 月报 */
const columns_2_0 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    { title: '本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
    { title: '上年同期', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
]
/** 限额以上住宿和餐饮业 - 季报 */
const columns_3_0 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    { title: '本季', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
    { title: '上年同期', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
]
/** 限额以上住宿和餐饮业 - 月报 */
const columns_3_1 = [
    {
        title: '指标名称',
        key: 'name',
        dataIndex: 'name',
        align: 'left',
        color: '#000',
        width: 300,
    },
    { title: '计量单位', key: '', dataIndex: 'currentValue', align: 'center', customRender: () => '千元' },
    { title: '代码', key: 'code', dataIndex: 'code', align: 'center', customRender: () => '300' },
    {
        title: '本年',
        align: 'center',
        children: [
            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center', color: '#5364bb' },
        ],
    },
    {
        title: '上年同期',
        align: 'center',
        children: [
            { title: '本月', key: '', dataIndex: 'currentValue', align: 'center' },
            { title: '1-本月', key: '', dataIndex: 'currentValue', align: 'center' },
        ],
    },
]
const columns = {
    columns_0_0,
    columns_0_1,
    columns_1_0,
    columns_1_1,
    columns_1_2,
    columns_2_0,
    columns_3_0,
    columns_3_1,
}
export const getColumns = (type, index) => {
    return columns[`${type}Columns${index}`]
}
