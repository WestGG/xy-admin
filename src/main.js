import { createApp } from 'vue'
import App from '@/App.vue'
import { useCore } from '@/core'
import mockServer from '../mock'

const app = createApp(App)
useCore(app)
app.mount('#app')
mockServer()
