import { UserOutlined } from '@ant-design/icons-vue'

export default [
    {
        path: 'role',
        name: 'role',
        component: 'role/index.vue',
        meta: {
            title: '用户管理',
            icon: UserOutlined,
            isMenu: true,
            keepAlive: true,
        },
    },
]
