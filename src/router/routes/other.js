import { BarChartOutlined, UserOutlined, CarryOutOutlined, MonitorOutlined } from '@ant-design/icons-vue'

export default [
    {
        path: 'reportAnalysis/table_0/list_0',
        name: 'reportAnalysis',
        component: 'table/index.vue',
        meta: {
            icon: BarChartOutlined,
            title: '报表分析',
            isMenu: true,
            keepAlive: true,
        },
    },
    {
        path: `reportAnalysis/table_0/detail_0`,
        name: `detail01`,
        component: 'table/detail.vue',
        meta: {
            title: `报表分析-详情`,
            isMenu: false,
            keepAlive: true,
        },
    },
    {
        path: `reportAnalysis/table_0/report`,
        name: `report_01`,
        component: 'table/report.vue',
        meta: {
            title: `报表分析-企业报告`,
            isMenu: false,
            keepAlive: true,
        },
    },
    {
        path: 'kpi',
        name: 'kpi',
        component: 'exception/loading.vue',
        meta: {
            title: 'KPI指标分析',
            icon: CarryOutOutlined,
            isMenu: true,
            keepAlive: true,
        },
    },
    {
        path: 'forecast',
        name: 'forecast',
        component: 'exception/loading.vue',
        meta: {
            title: '预计预测',
            icon: MonitorOutlined,
            isMenu: true,
            keepAlive: true,
        },
    },
]
