import { TableOutlined } from '@ant-design/icons-vue'
import { industryEnum } from '@/enums/industry'
const getList0Router = (index, industryName, title) => {
    return [
        {
            path: `list_0`,
            name: `list_0_${index}`,
            component: index < 4 ? 'table/index.vue' : 'exception/loading.vue',
            meta: {
                title: title,
                tabTitle: `${industryName}-${title}`,
                isMenu: true,
                keepAlive: true,
            },
        },
        {
            path: `detail_0`,
            name: `detail${index}`,
            component: 'table/detail.vue',
            meta: {
                title: `${industryName}-${title}详情`,
                isMenu: false,
                keepAlive: true,
            },
        },
        {
            path: `report`,
            name: `report_${index}`,
            component: 'table/report.vue',
            meta: {
                title: `${industryName}-企业报告`,
                isMenu: false,
                keepAlive: true,
            },
        },
    ]
}

const getList1Router = (index, industryName, title) => {
    return [
        {
            path: `list_1`,
            name: `list_1_${index}`,
            component: index < 4 ? 'table/index.vue' : 'exception/loading.vue',
            meta: {
                title: title,
                tabTitle: `${industryName}-${title}`,
                isMenu: true,
                keepAlive: true,
            },
        },
        {
            path: `detail_1`,
            name: `detail_1_${index}`,
            component: 'table/detail.vue',
            meta: {
                title: `${industryName}-${title}详情`,
                isMenu: false,
                keepAlive: true,
            },
        },
    ]
}

const getRouterItem = (index) => {
    const industryItem = industryEnum[`table_${index}`]
    const childrenItem = industryItem.children
    const industryName = industryItem.title
    const list_0 = childrenItem.list_0
    const list_1 = childrenItem.list_1
    const children = []
    if (list_0) children.push(...getList0Router(index, industryName, list_0.title))
    if (list_1) children.push(...getList1Router(index, industryName, list_1.title))
    return {
        path: `table_${index}`,
        name: `listTable_${index}`,
        component: 'RouteViewLayout',
        meta: {
            title: industryName,
            isMenu: true,
            keepAlive: true,
        },
        children,
    }
}
const createRouter = () => {
    const router = []
    for (let index = 0; index < 6; index++) {
        const curRouter = getRouterItem(index)
        router.push(curRouter)
    }
    return router
}
const reportListRouters = createRouter()
const reportCheckupRouter = {
    path: `reportCheckup`,
    name: `reportCheckup`,
    component: 'RouteViewLayout',
    meta: {
        icon: TableOutlined,
        title: '报表审核',
        isMenu: true,
        keepAlive: true,
    },
    children: reportListRouters,
}
/** 报表审核 */
export default reportCheckupRouter
