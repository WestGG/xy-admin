import { MessageFilled } from '@ant-design/icons-vue'

export default [
    {
        path: 'message',
        name: 'message',
        component: 'message/index.vue',
        meta: {
            icon: MessageFilled,
            title: '自动催报',
            isMenu: true,
            keepAlive: true,
        },
    },
]
