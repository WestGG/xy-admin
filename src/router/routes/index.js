// const modules = import.meta.glob('./*.js', { eager: true })
import reportCheckupRouter from './report'
import components from './components'
import roleRouter from './role'
import messageRouter from './message'
import otherRouter from './other'
const routes = []
routes.push(...messageRouter)
routes.push(reportCheckupRouter)
routes.push(...otherRouter)
routes.push(...roleRouter)
// routes.push(...components)
// routes.push(...homeRouter)

// Object.keys(modules).forEach((key) => {
//     const name = key.slice(key.lastIndexOf('/') + 1, key.lastIndexOf('.'))
//     if (name !== 'index') {
//         routes.push(...modules[key].default)
//     }
// })

export default routes
