import request from '@/utils/request'

// 登录
export const login = (params) => request.basic.post('/user/login', params)

export const regester = (params) => request.basic.post('/user/regester', params)
// 获取当前用户详情
export const getUserDetail = () => request.basic.get('/user/profile')
// 获取全部权限列表
export const getAuthList = () => request.basic.get('/permission/all')
