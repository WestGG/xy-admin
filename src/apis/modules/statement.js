import request from '@/utils/request'

/**  年报部分接口  */
// 获取分页列表
export const getList0 = (params) => request.basic.get('/statement/list', params)
// 报表详情
export const getDetail0 = (id) => request.basic.get(`/statement/detail`, { id })

export const getList = {
    list_0: getList0,
    list_1: getList0,
}
export const getDetail = {
    detail_0: getDetail0,
    detail_1: getDetail0,
}
