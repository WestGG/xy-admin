import request from '@/utils/request'

/**  年报部分接口  */
// 获取分页列表
export const getList0 = (params) => request.basic.get('/statement', params)
// 报表详情
export const getDetail0 = (id) => request.basic.get(`/statement/${id}`, {})
// 企业报告
export const getReport = (id) => request.basic.get(`/statement/${id}/report`)

/**  月报部分接口  */
// 获取分页列表
export const getList1 = (params) => request.basic.get('/statement/month', params)
// 报表详情
export const getDetail1 = (id) => request.basic.get(`/statement/month/${id}`, {})
export const getList = {
    list_0: getList0,
    list_1: getList1,
}
export const getDetail = {
    detail_0: getDetail0,
    detail_1: getDetail1,
}
