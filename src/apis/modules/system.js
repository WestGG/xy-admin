import request from '@/utils/request'

// 获取用户列表
export const getUserList = (params) => request.basic.get('/user', params)
// 创建用户
export const createUser = (params) => request.basic.post('/user', params)
// 更新用户
export const updateUser = (params) => request.basic.put('/user', params)
// 激活用户
export const reStartUser = (id) => request.basic.put(`/user/${id}/enable`)
// 禁用用户
export const delUser = (id) => request.basic.put(`/user/${id}/disable`)
// 获取全部权限列表
export const getAuthList = () => request.basic.get('/permission/all')

// 获取用户分页列表
export const getUserPageList = (params) => request.basic.get('/system/getUserPageList', params)
// 获取菜单列表
export const getMenuList = (params) => request.basic.get('/system/getMenuList', params)
// 获取新版菜单列表
export const getNewMenuList = (params) => request.basic.get('/system/getNewMenuList', params)
// 获取字典分类列表
export const getDictTypeList = (params) => request.basic.get('/system/getDictTypeList', params)
