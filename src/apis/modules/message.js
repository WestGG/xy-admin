import request from '@/utils/request'

/**  自动催报接口  */
// 获取短信列表
export const query = (params) => request.basic.get('/remind/sms', params)
// 创建短信
export const create = (params) => request.basic.post('/user', params)
// 更新短信
export const update = (params) => request.basic.put('/user', params)
// 推送短信
export const send = (id) => request.basic.put(`/user/${id}/enable`)
// 删除短信
export const del = (id) => request.basic.put(`/user/${id}/disable`)
// 上传手机号
export const uploadPhoneNum = ({ file }) => request.basic.post(`/remind/upload`, { file }, {})
// 下载手机号模板
export const downloadPhoneNum = () => request.basic.get(`/remind/sms/template/download`)
