import * as common from './modules/common'
import * as system from './modules/system'
import * as user from './modules/user'
import * as list from './modules/list'
import * as message from './modules/message'
import * as statement from './modules/statement'

export default {
    common,
    system,
    user,
    list,
    message,
    statement,
}
