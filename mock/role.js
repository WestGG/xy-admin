import { config } from '@/config'

const records = [
    {
        nickname: '刘建国',
        email: '123@email.com',
        permissions: ['statement', 'statement:export', 'user', 'user:query'],
        mobile: '18734562765',
        password: '123456',
    },
    {
        nickname: '王磊',
        email: '123@email.com',
        permissions: ['statement', 'statement:export', 'user', 'user:query'],
        mobile: '15623457777',
        password: '123456',
    },
    {
        nickname: '李天然',
        email: '123@email.com',
        permissions: ['statement', 'statement:export', 'user', 'user:query'],
        mobile: '132734568888',
        password: '123456',
    },
    {
        nickname: '王徐然',
        email: '123@email.com',
        permissions: ['statement', 'statement:export', 'user', 'user:query'],
        mobile: '18700000000',
        password: '123456',
    },
]

export default {
    code: config('http.code.success'),
    data: {
        records,
        pagination: {
            total: records.length,
            current: 1,
            pageSize: 10,
        },
    },
}
