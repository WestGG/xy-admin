import { config } from '@/config'
export default {
    code: config('http.code.success'),
    data: {
        token: 'mockToken',
        username: 'admin',
        userId: 1,
    },
}
