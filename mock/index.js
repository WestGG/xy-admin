// eslint-disable-next-line import/no-unresolved
import Mock from 'mockjs'
import signData from './sign.mock'
import userInfo from './userInfo'
import auth from './sign.mock'
import role from './role'
import list from './list'
export default function mockServer() {
    console.log('mock server start successfully!!')
    // Mock.mock('/api/user/login', signData)
    // Mock.mock('/api/user/profile', userInfo)
    // Mock.mock('/api/permission/all', auth)
    Mock.mock('/api/system/getUserList', role)
    Mock.mock('/api/data/list', list)
}
