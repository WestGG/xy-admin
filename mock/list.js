import { config } from '@/config'
const cardlabel = ['年初存货', '期末资产负债', '制造成本', '损益及分配', '人工成本、其他费用及增值税', '其他资料']
const cardlabel2 = ['本年折旧', '劳动者报酬', '营业盈余', '产生税净额']
const itemlabel2 = [
    [
        { label: '流动资产合计', children: ['应收账款', '预付账款', '存货', '存货产成品'] },
        '长期应收款',
        '长期股权投资',
        '投资性房地产',
        { label: '固定资产原价', children: ['房屋和构筑物', '机器和设备'] },
        { label: '固定资产累计折旧', children: ['本年折旧'] },
        '固定资产净值',
        '固定资产净额',
        { label: '使用权资产原价', children: ['房屋和构筑物', '机器设备'] },
        { label: '使用权资产累计折旧', children: ['本年折旧'] },
        '在建工程',
        { label: '无形资产', children: ['土地使用权', '软件使用权'] },
        '商誉',
        '资产总计',
        { label: '流动负债合计', children: ['应付账款', '预收账款'] },
        '租赁负债',
        '长期应付款',
        '负债合计',
        {
            label: '所有者权益合计',
            children: ['实收资本', '国家资本', '集体资本', '法人资本', '个人资本', '港澳台资本', '外商资本'],
        },
        '固定资产净值',
    ],
    [
        {
            label: '应付职工薪酬',
            children: [
                '工资、奖金、津贴和补贴',
                '福利费',
                '社保费',
                '住房公积金',
                '工会经费',
                '职工教育经费',
                '劳务派遣人员薪酬',
                '其他职工薪酬',
            ],
        },
        '其他劳动者报酬',
        '上交政府的各项非税费用',
        { label: '水电费', children: ['上缴的各项税费'] },
        '差旅费',
        '应交增值税',
        '销项税额',
        '进项税额',
    ],
    [
        { label: '营业收入', children: ['主营业务收入'] },
        '营业成本',
        '税金及附加',
        '销售费用',
        { label: '管理费用', children: ['上交管理费', '董事会费'] },
        '研发费用',
        { label: '财务费用', children: ['利息费用', '利息收入'] },
        '资产减值损失',
        '信用减值损失',
        '其他收益',
        '投资收益',
        '净敞口套期收益',
        '公允价值变动收益',
        '资产处置收益',
        '营业利润',
    ],
    ['营业外收入', '营业外支出', '利润总额', '所得税费用'],
]
const itemlabel = [
    [{ label: '年初存货', children: ['产成品'] }],
    [
        { label: '流动资产合计', children: ['应收账款', '预付账款', '存货', '存货产成品'] },
        '长期应收款',
        '长期股权投资',
        '投资性房地产',
        { label: '固定资产原价', children: ['房屋和构筑物', '机器和设备'] },
        { label: '固定资产累计折旧', children: ['本年折旧'] },
        '固定资产净值',
        '固定资产净额',
        { label: '使用权资产原价', children: ['房屋和构筑物', '机器设备'] },
        { label: '使用权资产累计折旧', children: ['本年折旧'] },
        '在建工程',
        { label: '无形资产', children: ['土地使用权', '软件使用权'] },
        '商誉',
        '资产总计',
        { label: '流动负债合计', children: ['应付账款', '预收账款'] },
        '租赁负债',
        '长期应付款',
        '负债合计',
        {
            label: '所有者权益合计',
            children: ['实收资本', '国家资本', '集体资本', '法人资本', '个人资本', '港澳台资本', '外商资本'],
        },
        '固定资产净值',
    ],
    ['直接材料消耗', '生产部门人员薪酬'],
    [
        { label: '营业收入', children: ['主营业务收入'] },
        '营业成本',
        '税金及附加',
        '销售费用',
        { label: '管理费用', children: ['上交管理费', '董事会费'] },
        '研发费用',
        { label: '财务费用', children: ['利息费用', '利息收入'] },
        '资产减值损失',
        '信用减值损失',
        '其他收益',
        '投资收益',
        '净敞口套期收益',
        '公允价值变动收益',
        '资产处置收益',
        '营业利润',
        '营业外收入',
        '营业外支出',
        '利润总额',
        '所得税费用',
    ],
    [
        {
            label: '应付职工薪酬',
            children: [
                '工资、奖金、津贴和补贴',
                '福利费',
                '社保费',
                '住房公积金',
                '工会经费',
                '职工教育经费',
                '劳务派遣人员薪酬',
                '其他职工薪酬',
            ],
        },
        '其他劳动者报酬',
        '上交政府的各项非税费用',
        { label: '水电费', children: ['上缴的各项税费'] },
        '差旅费',
        '应交增值税',
        '销项税额',
        '进项税额',
    ],
    ['工业总产值', '平均用工人数', '期末用工人数'],
]
const getNum = (max = 1) => Math.floor(Math.random() * max)
const getItemVal = () => {
    let value = getNum(300)
    if (value < 50) value = 60
    const realValue = value - getNum(30)
    const diffValue = realValue - value
    const checkValue = Math.floor((diffValue / value) * 100)

    const diff = Math.floor((diffValue / value) * 100) + '%'
    const remark = checkValue < -20 ? '需核实' : checkValue > -20 && checkValue < -10 ? '找不到' : '可以不审核'
    return {
        value,
        realValue,
        diff,
        diffValue,
        checkValue,
        remark,
        areaRage: getNum(300),
        tradeRage: getNum(300),
    }
}
const getItem = (cardIndex) => {
    const items = []
    // const item = itemlabel[cardIndex]
    const item = itemlabel2[cardIndex]
    item.forEach((v, i) => {
        const isString = typeof v == 'string'

        items.push({
            ...getItemVal(),
            label: isString ? v : v.label,
            children: !isString && v.children.map((i) => ({ ...getItemVal(), label: i })),
        })
    })
    return items
}
const getCardInfo = (cardIndex) => {
    const cardInfo = []
    cardInfo.push(...getItem(cardIndex))
    return cardInfo
}
// const info = cardlabel.map((v, i) => {
const info = cardlabel2.map((v, i) => {
    const value = getCardInfo(i)
    const all = { 本年折旧: '14606', 劳动者报酬: '14606', 营业盈余: '14606', 产生税净额: '14606' }
    return {
        label: v,
        all: all[v],
        value,
    }
})
const diyRecords = [
    {
        name: '远特信电子（深圳）有限公司',
        code: '91440300760495544B',
        userName1: '刘叶群',
        userName2: '徐炳华',
        phone: '33833288',
        date: '2024/2/29',
        info,
    },
    {
        name: '齐心商用设备（深圳）有限公司',
        code: '91440300789230726Y',
        userName1: '梁还莲',
        userName2: '邵新兴',
        phone: '17851183434',
        date: '2024/3/8',
        info,
    },
    {
        name: '深圳市建艺纸品有限公司',
        code: '91440300760483295C',
        userName1: '黄小琼',
        userName2: '罗伟彪',
        phone: '13509609667',
        date: '2024/2/28',
        info,
    },
    {
        name: '深圳新宙邦科技股份有限公司',
        code: '91440300736252008C',
        userName1: '黄瑶',
        userName2: '莫婷婷',
        phone: '13510415142',
        date: '2024/2/27',
        info,
    },
]
const record = {
    name: '深圳市众城丰电子有限公司',
    code: '91440300061437775K',
    userName1: '王林楠',
    userName2: '黎滔',
    phone: '13728856913',
    date: '2024/3/6',
    info,
}
const records = [...diyRecords].concat(...new Array(3).fill(record))

export default {
    code: config('http.code.success'),
    data: {
        records,
        pagination: {
            total: records.length,
            current: 1,
            pageSize: 10,
        },
    },
}
