export default () => ({
    host: '0.0.0.0',
    strictPort: false,
    proxy: {
        '/api': {
            target: 'https://report.lingjingshangdian.com/api',
            // target: 'http://localhost:8080',
            changeOrigin: true,
            rewrite: (path) => path.replace('/api', ''),
        },
    },
})
